/*  Skoczen Mike 2018
 *  Relay module
 */

 #include "Relay.h"

 Relay::Relay(int IN /* = 6 */){
  _IN = IN;
  Serial.println(_IN);
  pinMode(_IN, OUTPUT);
 }
 
void Relay::ON(){
  digitalWrite(_IN, HIGH);
  Serial.println("relay on");
 }
 
void Relay::OFF(){
  digitalWrite(_IN, LOW);
  Serial.println("relay off");
 }

