/*  Skoczen Mike 2018
 *  Relay header file
 */

#ifndef Relay_h
#define Relay_h

#include "Arduino.h"

class Relay{
  public:
  Relay(int IN = 6);
  void ON();
  void OFF();
  
  private:
  int _IN;
  
};
#endif;

