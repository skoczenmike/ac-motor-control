/*  Skoczen Mike 2018
 *  Optocoupler slot sensor
 *  have VCC 3,3 - 5 V GND, D0 - digital otuput, A0 - analog output
 */

#include "SlotSensor.h"

SlotSensor::SlotSensor(int D0 /* = 7 */){
  
  _D0 = D0;
  _step = 0;
  _state = false;
  
  pinMode(_D0,INPUT);
}

bool SlotSensor::UpdateState(){
  _state = digitalRead(_D0);
  return _state;
}

bool SlotSensor::ChangeState(){
  UpdateState();
  bool oldState = _state;
  while(oldState == _state){
    UpdateState();
  }
  return true; // state is change
}

int SlotSensor::StepsCount(int count){
  _step = 0;
  
  while(_step < count){
    ChangeState(); // 0 to 1 || 1 to 0
    ChangeState(); // 1 to 0 || 0 to 1
    // one step is upp;
    _step++;
    Serial.println(_step);
  }
  
  return _step;
}


