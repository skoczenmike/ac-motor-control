/*  Skoczen Mike 2018
 *  Encoder liblary header file
 */

#ifndef Encoder_h
#define Encoder_h

#include "Arduino.h"


class Encoder{

  public:
  Encoder(int CLK = 10, int DT = 11, int SW = 12);
  bool ButtonPressed();
  bool RotationChange();
  String DirectionOfRotation();
  
  private:
  int _CLK;
  int _DT;
  int _SW;
  bool _clkState;
  bool _dtState;
  bool _buttonState;

};
#endif
