/*  Skoczen Mike 2018
 *   Arduino project for controling AC motor rotate
 */

// Encoder modlule and variables
#include "Encoder.h"
const int Enc_clockPin    = 10;
const int Enc_dataPin     = 11;
const int Enc_switchPin   = 12;
Encoder* encoder;

// Display module and variables
#include "Display.h"
const int Dis_clockPin    = 8;
const int Dis_dataPin     = 9;
Display* display;

// Slot sensor module and variables
#include "SlotSensor.h"
const int SlotSensor_dataPin  = 7;
SlotSensor* slotsensor;

// Relay module starting engine 230V
# include "Relay.h"
const int Rel_inPin       =  6;
Relay* relay;

//The number is changed using the encoder
int intNumber;
int downNumber;

void MotorStop(){
  relay->OFF();
  display->Print(intNumber);
  Serial.println("Motor is stop");
}

void MotorStart(){
  display->Clear();
  Serial.println("Motor is run");
  relay->ON();
  int result = slotsensor->StepsCount(intNumber);
  Serial.print("steps sensor: ");
  Serial.println(result);
  MotorStop();
}

void PrintNumberOnLCD(int number){
  Serial.print("Number: ");
  Serial.println(number);
  display->Print(number);
}

//Function that checks the range of the number, maximum 999 and minimum 0
void ValidateRangeNumber(){
  if(intNumber > 999){
    intNumber = 0;
  }else if(intNumber < 0){
    intNumber = 999;
  }
}

void setup() {
  intNumber   = 10;
  encoder     = new Encoder(Enc_clockPin, Enc_dataPin, Enc_switchPin);
  display     = new Display(Dis_clockPin, Dis_dataPin);
  slotsensor  = new SlotSensor(SlotSensor_dataPin);
  relay       = new Relay(Rel_inPin);
  
  PrintNumberOnLCD(intNumber); // first print number if not LCD is off
  
  Serial.begin(9600);
}

void loop() {
  
  if(encoder->ButtonPressed()){
    MotorStart();
  }
  else if(encoder->RotationChange()){
    if(encoder->DirectionOfRotation() == "Left"){
      intNumber--;
    }else if(encoder->DirectionOfRotation() == "Right"){
      intNumber++;
    }
    
    ValidateRangeNumber(); // value 999 -:- 0
    PrintNumberOnLCD(intNumber);
    
    delay(60); //eliminates contact vibrations (encoder rotation)
  }
}
