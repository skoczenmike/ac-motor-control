/*  Skoczen Mike 2018
 *  Encoder library for:
 *  ENCODER BASCOM AVR have pins: CLK, DT, SW, +, GND.
 *  And thre resistors 10 000 ohm
 *  Button logic is reverse
 */

#include "Encoder.h"

Encoder::Encoder(int CLK /*= 10*/, int DT /*= 11*/, int SW /*= 12*/){
  _CLK = CLK;
  _DT = DT;
  _SW = SW;

  pinMode(_CLK,   INPUT);
  pinMode(_DT,    INPUT);
  pinMode(_SW,    INPUT);

  _clkState = true;
  _dtState = true;
  _buttonState = false;
}

bool Encoder::ButtonPressed(){
 _buttonState = digitalRead(_SW);
  // my encoder have reverse logic HIGH=false LOW=true
  if(_buttonState == HIGH){
    return false; //no pressed
  }else{
    return true;  //yes, pressed
  }
}

bool Encoder::RotationChange(){
  _dtState = digitalRead(_DT);
  _clkState = digitalRead(_CLK);
  
  if (_clkState == LOW ){
    return true; //Rotation change
  }else{
    return false;//Rotation stable
  }
}

String Encoder::DirectionOfRotation(){
  if((_clkState == LOW) && (_dtState == LOW)){
    return "Right";
  }
  else if ((_clkState == LOW) && (_dtState == HIGH)){
    return "Left";
  }
}




