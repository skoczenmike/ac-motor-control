/*  Skoczen Mike 2018
 *  Display class for:
 *  RobotDyn 7-segments LED display
 *  TM1637
 */

#include "Display.h"

Display::Display(int CLK /* = 8 */, int DIO /* = 9 */){
  _CLK = CLK;
  _DIO = DIO;
  
  tm1637 = new TM1637(_CLK, _DIO);
  tm1637->set(BRIGHT_TYPICAL);
  tm1637->init(D4056A);
}

void Display::Print(int number){
  tm1637->display(number);
}
void Display::Clear(){
  tm1637->clearDisplay();
}
