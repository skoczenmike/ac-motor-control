/*  Skoczen Mike 2018
 *  LED Display 7-segments header file
 */

#ifndef Display_h
#define Dislpay_h

#include "Arduino.h"
#include "TM1637.h"

class Display{
  public:
  Display(int CLK = 8, int DIO = 9);
  void Print(int number = 0);
  void Clear();
  
  private:
  int _CLK;
  int _DIO;
  TM1637* tm1637;
  
};
#endif
