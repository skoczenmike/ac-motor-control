/*  Skoczen Mike 2018
 *  Optocoupler slot sensor header file
 */

#ifndef SlotSensor_h
#define SlotSensor_h

#include "Arduino.h"

class SlotSensor{
  public:
  SlotSensor(int D0 = 7);
  int StepsCount(int count);
  
  private:
  int _D0;
  int _step;
  bool _state;
  bool UpdateState();
  bool ChangeState();
  
};
#endif
